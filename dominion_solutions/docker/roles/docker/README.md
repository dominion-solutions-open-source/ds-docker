Docker
=========

Installs docker on a server.

Requirements
------------

This role uses `curl`, `gnupg`, `ca-certificates` , and `python3-apt` to install everything that you will need.

Role Variables
--------------


Dependencies
------------

Example Playbook
----------------

```yml
---
- name: Apply Docker
  hosts: _docker
  roles:
    - docker
```

License
-------

MIT

Author Information
------------------

@spam-n-eggs
Dominion Solutions LLC - https://dominion.solutions
